import Register from "../../components/Register/Register";
import FusionHeader from "../../components/FusionHeader/FusionHeader";
import NavBar from "../../components/NavBar/NavBar";
import BreadCrumble from "../../components/BreadCrumble/BreadCrumble";
import Categories from "../../components/Filters/Categories/Categories";

import tShirt from "../../mock-api/V1/categories/1.json";

import "./Styles/PoductSchirt.css";

export default function ProductShirt() {
  const product = tShirt.items;

  return (
    <div>
      <Register />
      <FusionHeader />
      <NavBar className={"navBar"} />
      <BreadCrumble linkPage={"/camiseta"}> camisetas</BreadCrumble>
      <div className="page">
        <Categories />

        <div className="cards">
          <h3>CAMISETAS</h3>
          <div className="card_info">
            {product.map((element) => {
              return (
                <div>
                  <img scr={element.image} alt={element.name} />
                  <p>{element.name.toUpperCase()}</p>
                  <p>R$: {element.price}</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
