import Register from "../../components/Register/Register";
import FusionHeader from "../../components/FusionHeader/FusionHeader";
import NavBar from "../../components/NavBar/NavBar";
import BreadCrumble from "../../components/BreadCrumble/BreadCrumble";
import Categories from "../../components/Filters/Categories/Categories";

import shoes from "../../mock-api/V1/categories/3.json";

import "../Shirt/Styles/PoductSchirt.css";

export default function Shoes() {
  const product = shoes.items;

  return (
    <div>
      <Register />
      <FusionHeader />
      <NavBar className={"navBar"} />
      <BreadCrumble linkPage={"/calcados"}>sapatos</BreadCrumble>

      <div className="page">
        <Categories />

        <div className="cards">
          <h3>SAPATOS</h3>
          <div className="card_info">
            {product.map((element) => {
              return (
                <div>
                  <img scr={element.image} alt={element.name} />
                  <p>{element.name.toUpperCase()}</p>
                  <p>R$: {element.price}</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
