import Register from "../../components/Register/Register";
import FusionHeader from "../../components/FusionHeader/FusionHeader";
import NavBar from "../../components/NavBar/NavBar";
import BreadCrumble from "../../components/BreadCrumble/BreadCrumble";
import Categories from "../../components/Filters/Categories/Categories";

import pants from "../../mock-api/V1/categories/2.json";

import "../Shirt/Styles/PoductSchirt.css";

export default function Pants() {
  const product = pants.items;

  return (
    <div>
      <Register />
      <FusionHeader bars={"bars"} />
      <NavBar className={"navBar"} />
      <BreadCrumble linkPage={"/calcas"}>calças</BreadCrumble>
      <div className="page">
        <Categories />

        <div className="cards">
          <h3>CALCAS</h3>
          <div className="card_info">
            {product.map((element) => {
              return (
                <div>
                  <img
                    scr={`../../assets/${element.image}`}
                    alt={element.name}
                  />
                  <p>{element.name.toUpperCase()}</p>
                  <p>R$: {element.price}</p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}
