import Navbar from "../../../components/NavBar/NavBar";

import Image from "../../../assets/images/img.JPG";

export default function Content() {
  return (
    <>
      <Navbar className={"sideBar"} />
      <div className="seccaoMain">
        <img src={Image} alt="imagem padrao" />

        <div>
          <h1>Seja bem-vindo!</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non
            erat velit. Nullam non luctus turpis. Sed iaculis justo vel sapien
            finibus commodo. Aenean pharetra mauris id lorem sodales, in varius
            tortor gravida. Aenean quis hendrerit dui, at sagittis augue. Nunc
            placerat diam sit amet quam mollis dignissim. Ut libero nibh,
            feugiat eu mauris eu, venenatis condimentum mauris. Quisque vitae
            augue in mauris consequat elementum. Nunc auctor odio nibh, at
            ullamcorper elit feugiat dapibus. Proin imperdiet urna purus, eu
            ornare purus vestibulum vitae. Aenean pellentesque laoreet libero et
            scelerisque. Maecenas dictum blandit mi a rutrum. Ut bibendum nisl
            eu sapien euismod, eget facilisis velit hendrerit. Integer id leo at
            ex tempus commodo. Nulla suscipit commodo felis, sit amet volutpat
            odio tincidunt a. Ut posuere arcu non facilisis rhoncus. Class
            aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos. Cras augue urna, sollicitudin a mattis ac,
            ultricies tempor velit. Donec semper mi vel odio dignissim, et
            euismod eros consectetur. Nulla vel ultricies lectus. Cras non nibh
            justo. Suspendisse vulputate tellus at ligula aliquam faucibus.
          </p>
        </div>
      </div>
    </>
  );
}
