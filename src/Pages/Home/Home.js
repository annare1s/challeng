import Navbar from "../../components/NavBar/NavBar";
import Register from "../../components/Register/Register";
import FusionHeader from "../../components/FusionHeader/FusionHeader";
import Content from "./Content/Content";
import Footer from "../../components/Footer/Footer"

import "./Content/Style/Content.css";

export default function Home() {
  return (
    <>
      <header>
        <Register />
        <FusionHeader />
        <Navbar className={"navBar"} />
      </header>

      <main className="main">
        <Content />
      </main>
      <footer>
        <Footer/>
      </footer>
    </>
  );
}
