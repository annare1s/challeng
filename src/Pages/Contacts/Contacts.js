import Dev from "../../assets/images/anna.png";

import "./Styles/Contacts.css";

export default function Contact() {
  return (
    <div className="contact">
      <h1>Contato</h1>
      <img src={Dev} alt="Dev" />
      <h3>Anna Reis</h3>
      <p>Desenvolvedora Web Jr.</p>
      <div>
        <a href="https://www.linkedin.com/in/anna-beatriz-reis/">
          <i
            className="fab fa-linkedin"
            style={{ fontSize: "40px", color: "black" }}
          ></i>
        </a>

        <a href="https://github.com/AnnaRe1s">
          <i
            className="fab fa-github"
            style={{ fontSize: "40px", color: "black" }}
          ></i>
        </a>
      </div>
    </div>
  );
}
