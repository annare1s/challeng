import { Link } from "react-router-dom";

import menuBar from "../../mock-api/menu.json";

import "./Styles/NavBar.css";

export default function Navbar(props) {
  return (
    <div className={props.className === "navBar" ? "navBar" : "sideBar"}>
      <ul>
        {menuBar.map((element) => {
          return (
            <li>
              <Link to={element.link}>{element.name}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
