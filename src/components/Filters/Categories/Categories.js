import { Link } from "react-router-dom";

import list from "../../../mock-api/V1/categories/list.json";

import "./Styles/Categories.css";

export default function Categories() {
  const categoria = list.items;

  return (
    <div className="Categories">
      <h1>FILTRE POR</h1>
      <div>
        <h3>CATEGORIAS</h3>
        <ul>
          {categoria.map((element) => {
            return (
              <li>
                <Link to={`/${element.path}`}>{element.name}</Link>
              </li>
            );
          })}
        </ul>

        <div>
          <h3>CORES</h3>
          <button className="red"></button>
          <button className="orange"></button>
          <button className="blue"></button>
        </div>
      </div>

      <div>
        <h3>TIPO</h3>
        <ul>
          <li>
            <Link to="#"></Link>Corrida
          </li>
          <li>
            <Link to="#"></Link>Caminhada
          </li>
          <li>
            <Link to="#"></Link>Casual
          </li>
          <li>
            <Link to="#"></Link>Social
          </li>
        </ul>
      </div>
    </div>
  );
}
