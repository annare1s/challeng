import { useNavigate, Link } from "react-router-dom";
import { useState } from "react";

// Style
import "./Styles/FusionHeader.css";

// image
import logo from "../../assets/media/Logo.JPG";

export default function FusionHeader(props) {
  const [state, setState] = useState();

  const navegate = useNavigate();

  function handleChange(event) {
    let response = event.target.value;
    const search = response.toLowerCase();

    setState(search);
  }

  function handleSubmit(event) {
    event.preventDefault();
    navegate(`/${state}`);
  }

  return (
    <>
      <div className="headerBar">
        <span className="square">
          {props.bars === "bars"? (
            <i class="fas fa-bars"></i>
          ) : (
            <i class="fas fa-question"></i>
          )}
        </span>
        <Link to="/">
          <img src={logo} alt="WebJump logo" />
        </Link>

        <span className="squareRed  ">
          <i class="fas fa-question"></i>
        </span>

        <form onSubmit={handleSubmit} className="searchBar">
          <input onChange={handleChange} />
          <button type="submit" onSubmit={handleSubmit} className="btnSearch">
            BUSCAR
          </button>
        </form>
      </div>
    </>
  );
}
