import { Link } from "react-router-dom";

import "./Styles/Register.css";

export default function Register() {
  return (
    <div className="navbar">
      <Link to="/" style={{ color: "white", margin: "5px", fontWeight: "600" }}>
        Acesse sua Conta
      </Link>
      <p>ou</p>
      <Link to="/" style={{ color: "white", margin: "5px", fontWeight: "600" }}>
        Cadastre-se
      </Link>
    </div>
  );
}
