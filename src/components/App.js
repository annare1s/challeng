import { Routes, Route } from "react-router-dom";

import "../assets/Style.css";

import Home from "../Pages/Home/Home";
import Contact from "../Pages/Contacts/Contacts";
import Shirt from "../Pages/Shirt/ProductSchirt";
import Pants from "../Pages/Pants/Pants";
import Shoes from "../Pages/Shoes/Shoes";
import Page404 from "../Pages/Page404/Page404";

export default function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/camisetas" element={<Shirt />} />
      <Route path="/calcas" element={<Pants />} />
      <Route path="/calcados" element={<Shoes />} />
      <Route path="/contact" element={<Contact />} />
      <Route element={<Page404 />} />
    </Routes>
  );
}
