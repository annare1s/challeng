import { Link } from "react-router-dom";

import "./Styles/BreadCrumble.css";

export default function BreadCrumble(props) {
  return (
    <div className="breadCrumble">
      <Link to="/" className="principal">
        página inicial
      </Link>
      <i class="fas fa-chevron-right"></i>
      <Link to={props.linkPage} className="pageLink">
        {props.children}
      </Link>
    </div>
  );
}
