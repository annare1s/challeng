import axios from "axios";

const apis = "http://localhost:8888/api/V1/categories/list";

const api = axios.create({
  baseURL: apis[process.env.NODE_ENV],
});

export default api;
