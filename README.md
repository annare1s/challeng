# Challeng WebJump
This application was developed for a Web Jump test.
 
## About
It is a clothing and shoe store web application.
essa aplicacao foi desenvolvida em:
- React 
- Sass 

One thing I learned during this challenge was to learn more about webpack

 
### Install

To run application on your local machine you need it.
clone the project and execute the commands below.


```sh
$ git clone https://AnnaRe1s@bitbucket.org/annare1s/challeng.git
$ cd challeng
$ npm install
$ npm start
```


If after npm start does not automatically open the application.
Open a browser and type
- htpp://localhost:3000